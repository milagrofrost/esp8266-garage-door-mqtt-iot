#include <ESP8266WiFi.h>
#include <Ticker.h>
#include <AsyncMqttClient.h>
#include <NewPingESP8266.h>
#include <string>
#include <AsyncElegantOTA.h>


#define WIFI_SSID "xxxxxxxxxxxx"
#define WIFI_PASSWORD "xxxxxxxxxxxx"


#define MQTT_HOST IPAddress(10, 10, 101, 232)
#define MQTT_PORT 1883

#define MAX_DISTANCE 400

#define TRIGGER_PIN  16  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     14  // Arduino pin tied to echo pin on the ultrasonic sensor.

#define RELAY_PIN    4
#define WIFI_LED_PIN    0
#define MQTT_LED_PIN    2



unsigned long _time;
String doorStatus;
int enableRelay = 0;
int enableLED = 0;
int cooldown = -15000;
// cooldown is negative so that the relay can be enabled right at boot, if requested and not have to wait 15 seconds after boot.
//Since it's being compared to millis() which starts at 0.


NewPingESP8266 sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPingESP8266 setup of pins and maximum distance.
AsyncMqttClient mqttClient;
Ticker mqttReconnectTimer;

WiFiEventHandler wifiConnectHandler;
WiFiEventHandler wifiDisconnectHandler;
Ticker wifiReconnectTimer;

AsyncWebServer server(80);

void connectToWifi() {
  Serial.println("Connecting to Wi-Fi...");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
}

void onWifiConnect(const WiFiEventStationModeGotIP& event) {
  Serial.println("Connected to Wi-Fi.");
  digitalWrite(WIFI_LED_PIN, 1);
  connectToMqtt();
}

void onWifiDisconnect(const WiFiEventStationModeDisconnected& event) {
  Serial.println("Disconnected from Wi-Fi.");
  digitalWrite(WIFI_LED_PIN, 0);
  mqttReconnectTimer.detach(); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
  wifiReconnectTimer.once(2, connectToWifi);
}



void connectToMqtt() {
  Serial.println("Connecting to MQTT...");
  mqttClient.connect();
}

void onMqttConnect(bool sessionPresent) {
  Serial.println("Connected to MQTT.");
  Serial.print("Session present: ");
  Serial.println(sessionPresent);

  uint16_t packetIdSub = mqttClient.subscribe("garageDoor/action", 2);

  mqttClient.publish("garageDoor/uptime", 2, true, "init");
  mqttClient.publish("garageDoor/distance", 2, true, "init");
  mqttClient.publish("garageDoor/state", 2, true, "init");
  mqttClient.publish("garageDoor/action", 2, true, "init");
  mqttClient.publish("garageDoor/last_action", 2, true, "init");
  mqttClient.publish("garageDoor/last_action_result", 2, true, "init");
  mqttClient.publish("garageDoor/ip_address", 2, true, "init");

}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
  Serial.println("Disconnected from MQTT.");

  if (WiFi.isConnected()) {
    mqttReconnectTimer.once(2, connectToMqtt);
  }
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos) {
  Serial.println("Subscribe acknowledged.");
}

void onMqttMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {

  enableLED = 1;
  Serial.println("Remote Publish Detected.");
  Serial.print("  topic: ");
  Serial.println(topic);

  // Cleaning up Payload of remote published data

  char payloadClean[len];

  memcpy(payloadClean,&payload[0],len);
  payloadClean[len] = '\0';

  Serial.print("  payloadClean: ");
  Serial.println(payloadClean);

  // Serial.println(strcmp(payloadClean, "open"));
  // Serial.println(strcmp(payloadClean, "shut"));


// open logic
  if (strcmp(payloadClean, "open") == 0) {
    mqttClient.publish("garageDoor/last_action", 2, true, "open");
    if (doorStatus == "open") {
      Serial.println("Door's already open.");
      mqttClient.publish("garageDoor/last_action_result", 2, true, "Door's already open.");
    } else if (doorStatus == "shut") {

      //Make sure we're not sending relay commands too rapidly.
      if (millis() - cooldown < 15000 ){
        Serial.println("Still cooling down");
        mqttClient.publish("garageDoor/last_action_result", 2, true, "Cooldown timer");
      } else {
        Serial.println("Opening door");
        mqttClient.publish("garageDoor/last_action_result", 2, true, "Opening door");
        enableRelay = 1;
        cooldown = millis();
      }

    } else {
      Serial.println("Error");
      mqttClient.publish("garageDoor/last_action_result", 2, true, "Error");
    }
    mqttClient.publish("garageDoor/action", 2, true, "ack");
  }

// shut logic
  if (strcmp(payloadClean, "shut") == 0) {
    mqttClient.publish("garageDoor/last_action", 2, true, "shut");
    if (doorStatus == "shut") {
      Serial.println("Door's already shut.");
      mqttClient.publish("garageDoor/last_action_result", 2, true, "Door's already shut.");
    } else if (doorStatus == "open") {

      //Make sure we're not sending relay commands too rapidly.
      if (millis() - cooldown < 15000 ){
        Serial.println("Still cooling down");
        mqttClient.publish("garageDoor/last_action_result", 2, true, "Cooldown timer");
      } else {
        Serial.println("Closing door");
        mqttClient.publish("garageDoor/last_action_result", 2, true, "Closing door");
        enableRelay = 1;
        cooldown = millis();
      }

    } else {
      Serial.println("Error");
      mqttClient.publish("garageDoor/last_action_result", 2, true, "Error");
    }
    mqttClient.publish("garageDoor/action", 2, true, "ack");
  }


}

void onMqttPublish(uint16_t packetId) {
  Serial.println("Publish acknowledged.");
}




void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.println();

  pinMode(RELAY_PIN, OUTPUT);
  pinMode(WIFI_LED_PIN, OUTPUT);
  pinMode(MQTT_LED_PIN, OUTPUT);
  digitalWrite(RELAY_PIN, 0);
  digitalWrite(WIFI_LED_PIN, 0);
  digitalWrite(MQTT_LED_PIN, 0);

  wifiConnectHandler = WiFi.onStationModeGotIP(onWifiConnect);
  wifiDisconnectHandler = WiFi.onStationModeDisconnected(onWifiDisconnect);

  mqttClient.setServer(MQTT_HOST, MQTT_PORT);
  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.onSubscribe(onMqttSubscribe);
  mqttClient.onMessage(onMqttMessage);
  mqttClient.onPublish(onMqttPublish);

  connectToWifi();

  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, "text/plain", "Hi! I am ESP8266.");
  });

  AsyncElegantOTA.begin(&server);    // Start ElegantOTA
  server.begin();
  Serial.println("HTTP server started");

}




void loop() {

  AsyncElegantOTA.loop();

  //
  // Gather info and publish to MQTT
  //

  if ((millis()/1000) != _time) {
   _time = millis()/1000;
   Serial.println(uptime(millis()));
  }
  delay(1000);

  mqttClient.publish("garageDoor/uptime", 2, true, uptime(millis()));

  Serial.print("Distance: ");
  int distance = sonar.ping_cm();
  Serial.print(distance);
  Serial.println("cm\n");

  mqttClient.publish("garageDoor/distance", 2, true, (char*) String(distance).c_str());

  if ((distance < 15) && !(distance == 0)) {
    doorStatus = "open";
  } else {
    doorStatus = "shut";
  }

  mqttClient.publish("garageDoor/state", 2, true, (char*) String(doorStatus).c_str());

  mqttClient.publish("garageDoor/ip_address", 2, true, (char*) String(WiFi.localIP().toString()).c_str());

  //
  // Enable relay if variable enableRelay has been set by onMessage funciton
  //

  Serial.print("Enable relay=");
  Serial.println(enableRelay);
  if (enableRelay == 1 ){
    digitalWrite(RELAY_PIN, HIGH);
    delay(500);
    digitalWrite(RELAY_PIN, LOW);

    enableRelay = 0;
  }

  if (enableLED == 1 ){
    digitalWrite(MQTT_LED_PIN, 1);
    delay(500);
    digitalWrite(MQTT_LED_PIN, 0);

    enableLED = 0;
  }

}


//
// For the purpose of gathering total uptime info
//

char *uptime() // Function made to millis() be an optional parameter
{
  return (char *)uptime(millis()); // call original uptime function with unsigned long millis() value
}

char *uptime(unsigned long milli)
{
  static char _return[32];
  unsigned long secs=milli/1000, mins=secs/60;
  unsigned int hours=mins/60, days=hours/24;
  milli-=secs*1000;
  secs-=mins*60;
  mins-=hours*60;
  hours-=days*24;
  sprintf(_return,"Uptime %d days %2.2d:%2.2d:%2.2d.%3.3d", (byte)days, (byte)hours, (byte)mins, (byte)secs, (int)milli);
  return _return;
}
