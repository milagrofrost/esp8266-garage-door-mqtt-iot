# ESP8266 Garage Door MQTT IoT

https://imgur.com/a/NI9IVwr

There looks to be plenty of people that have done this before, but I did things a little differently for some reason.

It's been a few months since I put this together so I may have some missing or incorrect details in here. Apologies if you're using this as the only reference for your setup. (Don’t do that)

My biggest reason to have this setup is that we forget ALL THE TIME to close the garage door at night. Plus we have a car on the driveway that we don't want to have a garage remote in the car. So it would be nice to use the Home Assistant app to close or open remotely.

High level setup is that I have an ESP8266 connected to a relay that’s wired to a spare remote garage door opener that replaces the manual button function. The ESP8266 publishes MQTT packets to a locally hosted Mosquito server. Then my locally hosted Home Assistant server watches the MQTT data for garage status and changes. Home Assistant also is able to publish MQTT data back to the Mosquito server that, in turn, the ESP8266 watches for to know if it needs to toggle the garage door relay.

To have the most accurate distance for where the door is, I made a flat plate that travels with the door and a SR04 sensor that’s pointed directly at it and mounted magnetically to the garage door rail. The future plan is to maybe have some automation that can set and verify that the door is slightly cracked for our cat for those cold nights.

The ESP8266 has logic that determines what distance the SR04 reports is considered open or closed. Since the flat plate sits pretty close to the sensor, anything closer than 15 cm is considered an open door. (Should have variablized it) https://gitlab.com/milagrofrost/esp8266-garage-door-mqtt-iot/-/blob/main/ESP8266-garage-mqtt.c#L246

There’s logic on the ESP8266 to not accept any other commands for 15 seconds from when an inital command is sent. This is helpful when you use the IOS Home Assistant app where I had a tendancy to accidently toggle the door twice or more. Of course, this will also prevent you from undoing a mistake quickly. That could be a downside.

The Home Assistant automation will close the door at 11:40 PM, not before it flashes the exterior garage lights a few times, so hopefully we won’t be caught off guard if we’re outside at that time.

I made, printed and am using the 3D objects for the ESP8266/relay enclosure, the flat plate that travels and the SR04 holder/bracket. All of this is included in the repository.

Sorry, I don’t have pictures of the wiring. :(

I publish more MQTT data from the ESP8226 than what Home Assistant ingests. This is strictly for troubleshooting and general curiosity. This includes uptime, distance, last_action, last_action_result and distance.

I’ve included the Home Assistant configuration and automation examples in the repository.

Why do I wire my relay to a spare remote instead of directly to the garage door motor pins? Unlike older garage motors, the garage door I have doesn’t have a set of pins that you can short that will toggle the door. The pins on the back of this garage door motor that the wall mounted opener, are only for powering the wall mounted opener. The wall mounted opener actually is another garage door remote.

